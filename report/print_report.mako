## -*- coding: utf-8 -*-
<html>
  <head>
    <style type="text/css">
    ${css}

    body, table {
        font-family: helvetica;
        font-size: 9px;
    }

    .header {
        margin-left: 0px;
        text-align: left;
        font-size: 10px;
    }

    .title {
        font-size: 14px;
        font-weight: bold;
    }

    .list_table {
        text-align: center;
        border-collapse: collapse;
    }

    .list_table td {
        text-align: left;
        font-size: 10px;
    }

    .list_table th {
        font-size: 10px;
    }

    .list_table thead {
        display: table-header-group;
    }

    .address table {
        font-size: 9px;
        border-collapse: collapse;
        margin: 0px;
        padding: 0px;
    }

    .address .invoice {
        margin-top: 10px;
    }

    .address .recipient {
        margin-right: 120px;
        float: right;
    }

    table .address_title {
        font-weight: bold;
    }

    .address td.name {
        font-weight: bold;
    }

    td.amount, th.amount {
        text-align: right;
        white-space: nowrap;
    }

    td.desc, th.desc {
        text-align: left;
    }

    h1 {
        font-size: 13px;
        font-weight: bold;
    }

    tr.line .note {
        font-size: 8px;
    }

    tr.line {
        margin-bottom: 10px;
    }
    /*
     * Mi Css
    */

    .act_as_table {
        display: table;
        width: 100%;
        text-align:center;
        table-layout: fixed;
        page-break-inside: avoid

    }

    .act_as_row  {
        display: table-row;
    }

    .act_as_cell {
        display: table-cell;
        text-align:left;
        font-size:10px;
        padding-right:3px;
        padding-left:3px;
        padding-top:5px;
        padding-bottom:3px;
        clear:both;
    }

    .act_table,
    .act_as_cell,
    .act_as_row {
        word-wrap: break-word;
    }

    .contenedor {
        color: red;
        font-size:100px;
        padding-top:350px;
        -webkit-transform: rotate(-45deg);
    }

    .contenedor_principal {
        position: relative;
        width: 100%;
    }

    .contenedor_contenido {
        position: relative;
        z-index: 2;
    }

    .contenedor_cancelado {
        position: absolute;
        top:0px;
        left:-60px;
        z-index: 1;
    }

    </style>
  </head>
<body>
  <%from datetime import date %>

  <%def name="address(partner)">
    ${partner.street}
    %if hasattr(partner, 'l10n_mx_street3') and partner.l10n_mx_street3:
      ${partner.l10n_mx_street3}
    %endif
    %if hasattr(partner, 'l10n_mx_street4') and partner.l10n_mx_street4:
       ${partner.l10n_mx_street4}
    %endif
    ${partner.street2 or ''},
    ${partner.city or ''}
    <span>C.P. </span>${partner.zip or ''}, ${partner.country_id.name or ''} ${partner.state_id.name or ''}
    </br>
    <span>RFC: ${partner.vat or ''}</span>
  </%def>

  <%def name="name(partner)">
      %if partner.parent_id:
          ${partner.parent_id.name or ''}
          ${partner.title and partner.title.name or ''}
          ${partner.name}
      %else:
          ${partner.title and partner.title.name or ''} ${partner.name}
      %endif
  </%def>

   <div class="act_as_table" style="padding-top:10px;">
      <div class="act_as_row">
          <div class="act_as_cell" style="border:1px solid black; width:25%;">
              <div style="position: absolute; top: 30px; bottom: 0px;">
                      <center>&nbsp; &nbsp; ${helper.embed_logo_by_name('logo',160)|n}</center>
              </div>
          </div>
          <div class="act_as_cell" style="border:1px solid black; width:60%">
              <div style="text-align:center; font-size:18px;">
                    <br>
                    <b>PLANEACION DE ENTREGAS PRODUCTO TERMINADO</b>
                    <br>
                    <br>
                    <br>
              </div>
          </div>
            <div class="act_as_cell" style="border:1px solid black; padding:0px">
                <div  class="act_as_table" style="text-align:center; font-size:12px;">
                    <div class="act_as_row">
                        <div class="act_as_cell" style="padding:5px">
                        </br>
                         FR-06-07
                        </br>
                        </br>
                        </div>
                    </div>
                    <div class="act_as_row" >
                        <div class="act_as_cell" style="padding:5px; border-top:1px solid black;">
                        </br> <div class="lolo" style="visibility:visible;">
                         Edición 1</b></div>
                        </br>
                        </div>
                    </div>
                </div>
             </div>
          </div>
      </div>
  </div>

  <%
  def carriage_returns(text):
      return text.replace('\n', '<br />')
  %>

  <!--&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; ${_('Done by')}: ${user.name}-->
  <br/>
  <div style="margin-left:80%; text-align:right">
  </div>
  <div style="text-align:center; font-size:18px; padding-top:10px;">

  </div>


  <div class="act_as_table" style="padding-top:10px;">
        <div class="act_as_row" style="">
          <div class="act_as_cell" style="border: 1px solid black; width:50px; text-align:center;font-size:12px;">
              <br>
              <b>Cliente</b>
          </div>
          <div class="act_as_cell" style="border: 1px solid black; width:10px; text-align:center; font-size:12px;">
              <br>
              <b>Referencia</b>
          </div>
          <div class="act_as_cell" style="border: 1px solid black; width:15px; text-align:center; font-size:12px;">
              <br>
              <b>Fecha Programada</b>
          </div>
          <div class="act_as_cell" style="border: 1px solid black; width:10px; text-align:center; font-size:12px;">
              <br>
              <b>Fecha de Entrega</b>
          </div>
          <div class="act_as_cell" style="border: 1px solid black; width:10px; text-align:center; font-size:12px;">
              <br>
              <b>Tiempo de entrega (OTO) %</b>
          </div>
        </div>
        <% cont_avg = 0.0 %>
        <% sum_avg = 0.0 %>
        <% total_avg = 0.0 %>

        %for line in get_data():
            <% avg = line['avg'] or 0.00 %>
            <div class="act_as_row">
              <div class="act_as_cell" style="border-bottom: 1px solid black; border-left: 1px solid black;text-align:left;  border-right: 1px solid black; padding:5px;">
                ${line['partner']}
              </div>
              <div class="act_as_cell" style="border-bottom: 1px solid black; border-left: 1px solid black;text-align:left;  border-right: 1px solid black; padding:5px;">
                ${line['reference']}
              </div>
              <div class="act_as_cell" style="border-bottom: 1px solid black; border-left: 1px solid black;text-align:left;  border-right: 1px solid black; padding:5px;">
                ${line['min_date']}
              </div>
              <div class="act_as_cell" style="border-bottom: 1px solid black; border-left: 1px solid black;text-align:left;  border-right: 1px solid black; padding:5px;">
                 ${line['date_done']}
              </div>
              <div class="act_as_cell" style="border-bottom: 1px solid black; border-left: 1px solid black;text-align:left;  border-right: 1px solid black; padding:5px;">
                 ${line['avg'] or '0.00'}
              </div>
            </div>

            <% cont_avg = cont_avg + 1 %>
            <% sum_avg = sum_avg + avg %>
        %endfor
            <!--Row de total-->

            <% total_avg = sum_avg / cont_avg %>
            <div class="act_as_row">
                  <div class="act_as_cell" style="border-bottom: 1px solid black; border-left: 1px solid black;text-align:left;  border-right: 1px solid black; padding:5px;">

                  </div>
                  <div class="act_as_cell" style="border-bottom: 1px solid black; border-left: 1px solid black;text-align:left;  border-right: 1px solid black; padding:5px;">

                  </div>
                  <div class="act_as_cell" style="border-bottom: 1px solid black; border-left: 1px solid black;text-align:left;  border-right: 1px solid black; padding:5px;">

                  </div>
                  <div class="act_as_cell" style="border-bottom: 1px solid black; border-left: 1px solid black;text-align:left;  border-right: 1px solid black; padding:5px;">

                  </div>
                  <div class="act_as_cell" style="border-bottom: 1px solid black; border-left: 1px solid black;text-align:left;  border-right: 1px solid black; padding:5px;">
                     <b>${formatLang(total_avg) or '0.00'}</b>
                  </div>
             </div>
        </div>
  </div>

  </body>
</html>