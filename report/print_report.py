# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Authors: Aktiva (<http://www.aktiva.com.mx/>)
#             
#    Coded by: llores@aktiva.com.mx
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

import openerp
from openerp.tools.translate import _
from openerp.report import report_sxw
import time
import datetime

class PrintDeliveryAvg(report_sxw.rml_parse):
    
    def __init__(self, cr, uid, name, context):
        super(PrintDeliveryAvg, self).__init__(cr, uid, name, context=context)

        self.cursor = self.cr

        self.localcontext.update({
            'get_period' : self._get_period,
            'get_company_emiter': self._get_company_emiter,
            'get_data': self._get_data,
            'time': time.strftime('%d de %B del %Y'),

        })

    def _get_period(self):
        period_id = self.datas['form']['period_id']
        period = self.pool.get('account.period').read(self.cr, self.uid, [period_id], ['name'], context=None)
        return period[0]['name']

    def _get_company_emiter(self):
        user = self.pool.get('res.users').read(self.cr, self.uid, self.uid, ["company_id"])
        company = user['company_id']
        company_id = user['company_id'][0]
        company_name = user['company_id'][1]
        return company

    def _get_full_name(self, partner_id):
        sql_query = """
            select pp.name, rp.name
            from res_partner rp Left JOIN res_partner pp
            ON rp.parent_id = pp.id
            where rp.id = %d
            """ % (int(partner_id))

        self.cr.execute(sql_query)
        result = self.cr.fetchall()
        name_parent= result[0][0]
        name_partner = result[0][1]
        if name_parent:
            return name_parent + ',' + name_partner
        return name_partner

    def _get_data(self):
        period_id = self.datas['form']['period_id']
        period_name = self.pool.get('account.period').read(self.cr, self.uid, [period_id], ['name'], context=None)[0]['name']
        month, year = period_name.split('/')
        scheduled_time = self.datas['form']['scheduled_time']
        delivery_date = self.datas['form']['date_delivery']
        if scheduled_time:
            sql_query = """
                SELECT sp.partner_id as partner, sp.name as reference, sp.min_date as min_date, sp.date_done as date_done, sp.avg_date_delivery as avg
                FROM stock_picking sp JOIN res_partner rp
                ON sp.partner_id = rp.id
                WHERE sp.type ='out'
                AND date_part('months', sp.min_date) = %d
                AND date_part('year', sp.min_date) = %d
                AND sp.state = 'done'
                AND sp.avg_date_delivery > 0
                AND sp.name not ILIKE '%s'
                AND sp.name not ILIKE '%s'
                ORDER BY sp.name DESC
                """ % (int(month),int(year),str('%devolver%'), str('INT%'))
        if delivery_date:
            sql_query = """
                SELECT sp.partner_id as partner, sp.name as reference, sp.min_date as min_date, sp.date_done as date_done, sp.avg_date_delivery as avg
                FROM stock_picking sp JOIN res_partner rp
                ON sp.partner_id = rp.id
                WHERE sp.type ='out'
                AND date_part('months', sp.date_done) = %d
                AND date_part('year', sp.date_done) = %d
                AND sp.state = 'done'
                AND sp.avg_date_delivery > 0
                AND sp.name not ILIKE '%s'
                AND sp.name not ILIKE '%s'
                ORDER BY sp.name DESC
                """ % (int(month),int(year),str('%devolver%'), str('INT%'))
        print sql_query
        self.cr.execute(sql_query)
        result = self.cr.dictfetchall()

        for line in result:
            line['partner'] = self._get_full_name(line['partner'] )
        return result

report_sxw.report_sxw(
    'report.webkit.ak_delivery_avg',
    'res.partner',
    'addons/ak_print_nub_delivery_avg/report/print_report.mako',
    parser=PrintDeliveryAvg,
)


