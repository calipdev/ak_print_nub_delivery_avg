# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Authors: Aktiva (<http://www.aktiva.com.mx/>)
#
#    Coded by: llores@aktiva.com.mx
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################


from openerp.osv import orm, osv, fields
from openerp.tools.translate import _

class delivery_avg_wizard(orm.TransientModel):
    _name = 'delivery.avg.wizard'
    _columns = {
        'fiscalyear_id': fields.many2one(
            'account.fiscalyear', 'Año Fiscal',
            help=_('Seleciona el Año fiscal'),
            required=True,
        ),
        'period_id': fields.many2one(
            'account.period', 'Periodo',
            help=_('Seleciona el periodo'),
            required=True,
        ),
        'scheduled_time': fields.boolean(
            'Fecha programada',
            ),
        'date_delivery': fields.boolean(
            'Fecha de entrega',
        ),
    }
    _defaults = {
        #'target_move': 'posted',
    }

    def change_fiscalyear_id(
        self, cr, uid, ids, fiscalyear_id=False, context=None
    ):
        """
        Update the period in the wizard based on the fiscal year
        """
        res = {}
        res['value'] = {}
        if fiscalyear_id:
            period = False
            cr.execute('''
                SELECT * FROM (SELECT p.id
                               FROM account_period p
                               LEFT JOIN account_fiscalyear f ON
                               (p.fiscalyear_id = f.id)
                               WHERE f.id = %s
                               ORDER BY p.date_start ASC
                               LIMIT 1) AS period_start''' % fiscalyear_id)
            periods = [i[0] for i in cr.fetchall()]
            if periods and len(periods) > 0:
                period = periods[0]
            res['value'] = {'period_id': period}
        return res



    def pre_print_report(self, cr, uid, ids, data, context=None):
        res = self.browse(cr, uid, ids, context=context)[0]
        data['form'] = {
            'fiscalyear_id': res.fiscalyear_id.id,
            'period_id': res.period_id.id,
            'scheduled_time': res.scheduled_time,
            'date_delivery': res.date_delivery
        }
        return data

    def print_report(self, cursor, uid, ids, data, context=None):
        # we update form with display account value
        datas = self.pre_print_report(cursor, uid, ids, data, context=context)
        return {'type': 'ir.actions.report.xml',
                'report_name': 'webkit.ak_delivery_avg',
                'datas': datas,
                'context': {
                    'FileExt': 'pdf',
                    'FileName': "prueba"
                }
        }