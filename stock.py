# -*- encoding: utf-8 -*-
###########################################################################
#    Module Writen to OpenERP, Open Source Management Solution
#
#    Authors: Aktiva (<http://www.aktiva.com.mx/>)
#
#    Coded by: llores@aktiva.com.mx
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
##############################################################################

from openerp.osv import osv, orm, fields
from openerp import netsvc
import time
import datetime

from openerp.tools.translate import _
import openerp.addons.decimal_precision as dp

class stock_picking(orm.Model):
    _inherit = 'stock.picking'

    def _get_avg_date_delivery(self, cr, uid, ids, name, args, context=None):
        res = {}
        for record in self.browse(cr, uid, ids, context=context):

            if record.date_done and record.min_date:

                date_done_str = str(record.date_done).split(" ")[0]
                date_done = datetime.datetime.strptime(date_done_str, '%Y-%m-%d').date()
                min_date_str = str(record.min_date).split(" ")[0]
                min_date = datetime.datetime.strptime(min_date_str, '%Y-%m-%d').date()
                days = (date_done - min_date).days
                #avg_date = 0
                if days <= 1:
                    avg_date = 100
                elif 2 <= days <= 3:
                    avg_date = 95
                elif 4 <= days <= 5:
                    avg_date = 90
                elif 6 <= days <= 7:
                    avg_date = 85
                elif 8 <= days <= 9:
                    avg_date = 80
                elif 10 <= days <= 11:
                    avg_date = 75
                elif 12 <= days <= 13:
                    avg_date = 70
                elif 14 <= days <= 15:
                    avg_date = 65
                elif days >= 16:
                    avg_date = 60
                res[record.id] = avg_date
            else:
                res[record.id] = 0
        return res

    _columns = {
        'avg_date_delivery': fields.function(_get_avg_date_delivery, type='integer', string='Tiempo de Entrega(OTO)%', store=True),
    }

class stock_picking_out(orm.Model):
    _inherit = 'stock.picking.out'

    def _get_avg_date_delivery(self, cr, uid, ids, name, args, context=None):
        res = {}
        for record in self.browse(cr, uid, ids, context=context):

            if record.date_done and record.min_date:
                date_done_str = str(record.date_done).split(" ")[0]
                date_done = datetime.datetime.strptime(date_done_str, '%Y-%m-%d').date()
                min_date_str = str(record.min_date).split(" ")[0]
                min_date = datetime.datetime.strptime(min_date_str, '%Y-%m-%d').date()
                days = (date_done - min_date).days
                #avg_date = 0
                if days <= 1:
                    avg_date = 100
                elif 2 <= days <= 3:
                    avg_date = 95
                elif 4 <= days <= 5:
                    avg_date = 90
                elif 6 <= days <= 7:
                    avg_date = 85
                elif 8 <= days <= 9:
                    avg_date = 80
                elif 10 <= days <= 11:
                    avg_date = 75
                elif 12 <= days <= 13:
                    avg_date = 70
                elif 14 <= days <= 15:
                    avg_date = 65
                elif days >= 16:
                    avg_date = 60
                res[record.id] = avg_date
            else:
                res[record.id] = 0
        return res

    _columns = {
        'avg_date_delivery': fields.function(_get_avg_date_delivery, type='integer', string='Tiempo de Entrega(OTO)%', store=True),
    }


# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
